import ballerina/grpc;
import ballerina/log;

@grpc:ServiceDescriptor {
    descriptor:  ROOT_DESCRIPTOR_MANAGER
}



service "Management" on new grpc:Listener(9090) {

     private table<User> key(id) users;
     private table<Student> key(std_num) students;
     private table<Assignment> key(student) assignments;
     private Student theStudent;
     
       



     function  init() {
        
        self.students = table[];
        self.users = table[];
        self.assignments = table[];
     }

    remote function hello(string request) returns string|error {
        // Reads the request message and sends a response.
        return "Hello " + request;
    }

 #   remote function create_user(User user) returns string|error {
#            
#             self.users.add(user);
#           
#            return "-------------------------------\n"+
#                   "|       User's Details        |\n"+
#                   "-------------------------------\n"+
#                   "User: "+user.toString();
#
#    }


remote function create_user(stream<User, error?> user) returns string|error {

           

            log:printInfo("Client connected successfully.");


             _ = check from User u in user
            do {
                log:printInfo(string `Greet received: ${u.name}`);

                self.users.add(u);
                
            };

              
                         
            return "ACK";

    }




     remote function login(pswd p) returns string|error {
            
            string resp;

           // User? theUser = self.users[p.username];

            
                 return " ";
               
    }

    remote function getUser(string id) returns User?|error {
            
            string resp;

            User? theUser = self.users[id];
           // theUser = {id:"",name:"",address:"",profile_type: "",password:""};

            
                 return theUser;
               
    }

     remote function getAllUsers() returns string|error {

            string allUsers=self.users.toString();
               
    
            return "Registration was successful"+allUsers;
            
    }



    remote function setUser(User partUser) returns string|error {
            
            string resp;

            User? theUser = self.users[partUser.id];
            theUser = {id:partUser.id,
                      name:partUser.name,
                      address:partUser.address,
                      profile_type: partUser.profile_type,
                      password:partUser.password};

            
                 return theUser.toString();
               
    }




     remote function create_course(Course course) returns string|error {
            
            return "Course Created Successfully"+course.name;
            
    }

    

    

     remote function assign_course(Course course) returns string|error {
            
            return "User was created"+course.name;
            
    }

    remote function submit_assignment(Assignment a) returns string|error {
            
             self.assignments.add(a);   

            return a.course+" Submitted Successfully";
            
    }

         remote function request_assignment(string student) returns Assignment?|error {

           Assignment? a=self.assignments[student];

            return a;
            
        }

    remote function register(Student student) returns string {

             // Student theStudent = {std_num: student.std_num, name: student.name, courses: student.courses};

                  self.students.add(student);
    
            return "Registration was successful"+student.name;
            
    }

     remote function getAllStudents() returns string|error {

            string allStudents=self.students.toString();
               
    
            return allStudents;
            
    }

     remote function getStudent(string id) returns Student?|error {
            
                   Student? theStudent = self.students[id];
            
                 return theStudent;
               
    }







}

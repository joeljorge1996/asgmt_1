import ballerina/http;

public type student record {|
    readonly string std_number;
    string name;
    string email;
    string address;
    string[] courses;

|}; 

public type StudentCreated record {|
    *http:Created;
     student body;
|};

public type ErrorMessage record {|
    string errorMessage;
|};


public type StudentNotFound record {|
    *http:NotFound;
    ErrorMessage body;
|};

public type StdPart record {|
    string name?;
    string email?;
    string address?;
    string[] courses?;
|};



service /students on new http:Listener (9090){

    private table<student> key(std_number) students;
    
     function  init() {
        self.students = table[];
     }



    resource function post createStudent(@http:Payload student n) returns json|error{
    
        self.students.add(n);

        return {"Added":n.name};

    }

    resource function get getAll() returns student[]{
      
     return self.students.toArray();
              
    }

    resource function get getStudent/[string std_number]() returns student|StudentNotFound{

        student? theStudent = self.students[std_number];

        if theStudent == () {
             return <StudentNotFound>{
                body: {errorMessage: string `Error no student with number ${std_number}`}
            }; 
        } else {
            return theStudent;
            }

     }


     resource function put editCourse/[string std_number](@http:Payload StdPart stdPart) returns    StudentNotFound|student {

        
        student? theStudent = self.students[std_number];

        if theStudent == () {
             return <StudentNotFound>{
                body: {errorMessage: string `Error no student with number ${std_number}`}
            }; 
        } else {
        
           string? name = stdPart?.name;
           if name != (){
                theStudent.name = name;
            }

            string? email = stdPart?.email;
            if email != (){
                theStudent.email = email;
            }

            string? address = stdPart?.address;
            if address != () {
                theStudent.address = address;
            }

            string[]? courses = stdPart?.courses;
            if courses != (){
                theStudent.courses = courses;
            }
            return theStudent;

        }
     }

     resource function delete deleteStudent/[string std_number] () returns StudentNotFound|http:Ok {
        student? theStudent = self.students[std_number];

        if theStudent == () {
             return <StudentNotFound>{
                body: {errorMessage: string `Error no student with number ${std_number}`}
            }; 
        }else {
            http:Ok ok = {body: "Deleted Course"};
            return ok;
        }
    }




}

